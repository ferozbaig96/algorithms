#include <stdio.h>
#include <limits.h>
#include <time.h>

int p[10], n, m[10][10], s[10][10];

void find_mcm()
{
	int i,j,k,L,q;

	for(i = 1; i < n; i++)
		m[i][i] = 0;

	for(L = 2; L < n; L++)
	{
		for(i = 1; i < n-L+1; i++)
		{
			j = i+L-1;
			m[i][j] = INT_MAX;

			for(k = i; k < j; k++)				// k < j
			{
				q = m[i][k] + m[k+1][j] + p[i-1]*p[k]*p[j];

				if(q < m[i][j])
				{
					m[i][j] = q;
					s[i][j] = k;
				}
			}
		}
	}
}

void print_mcm(int i, int j)
{
	if (i == j)
		printf("A");

	else
	{
		printf("(");
		print_mcm(i , s[i][j]);
		print_mcm(s[i][j]+1 , j);
		printf(")");
	}
}

int main(int argc, char const *argv[])
{
	int i;
	clock_t begin, end;

	printf("Enter the array size : ");
	scanf("%d",&n);

	for(i = 0; i < n; i++)
		scanf("%d",&p[i]);

	begin = clock();

	find_mcm();

	printf("\nMinimum number of multiplications : %d\n", m[1][n-1]);
	print_mcm(1,n-1);

	end = clock();

	printf("\nExecution time : %lf\n",(double)(end-begin)/CLOCKS_PER_SEC);	

	return 0;
}
