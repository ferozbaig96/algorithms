#include <stdio.h>

char tasks[100], early_tasks[100];
int d[100], p[100], n, total_penalty;

void sort()
{
	int i,j,t;
	char temp;

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n-i; j++)
		{
			if (p[j] < p[j+1])
			{
				t = p[j];
				p[j] = p[j+1];
				p[j+1] = t;

				t = d[j];
				d[j] = d[j+1];
				d[j+1] = t;

				temp = tasks[j];
				tasks[j] = tasks[j+1];
				tasks[j+1] = temp;
			}
		}
	}
}

void find_early_tasks()
{
	int size = 0;		// represents size of selected early-tasks
	int i, t;

	t = d[0];
	early_tasks[0] = tasks[0];
	size++;

	total_penalty = 0;

	for ( i = 1; i < n; i++)
	{
		if ( d[i] > t)
		{
			t = d[i];

			early_tasks[size] = tasks[i];
			size++;
		}
		// d[i] <= t
		else if ( size < t)
		{			
			early_tasks[size] = tasks[i];
			size++;
		}

		else
			total_penalty += p[i];
	}
}

int main()
{
	int i;

	printf("Enter the no. of tasks : ");
	scanf("%d",&n);

	printf("Enter the deadline, penalty for all the tasks : \n");
	
	for (i = 0; i < n; ++i)
	{
		scanf("%d%d",&d[i], &p[i]);
		tasks[i] = (char)(65+i);
	}

	sort();
	find_early_tasks();

	printf("\nEarly tasks : ");

	for( i = 0; i < n; i++)
		printf("%c ",early_tasks[i]);

	printf("\nTotal penalty : %d\n",total_penalty);

	return 0;
}
