#include<stdio.h>
#include<alloc.h>
#include<conio.h>
#include<stdio.h>
struct tree
{
	int info;
	struct tree *left;
	struct tree *right;
};

struct tree *insert(struct tree *,int);
void inorder(struct tree *);
void postorder(struct tree *);
void preorder(struct tree *);
struct tree *delet(struct tree *,int);
struct tree *search(struct tree *);

void main()
{

	struct tree *root;
	int choice, item,item_no;
	root = NULL;
	while(1)
	{
		clrscr();
		printf("\n1. Insert in Binary Tree\n2. Delete from Binary Tree\n3. Traversal of a Binary tree\n4. Search and replace\n5. Exit\nEnter choice : ");
		scanf(" %d",&choice);
		switch(choice)
		{
			case 1:
				printf("\n Enter new element: ");
				scanf("%d", &item);
				root= insert(root,item);
				printf("\n root is %d",root->info);
				printf("\n Inorder traversal of binary tree is : ");
				inorder(root);
				getch();
				break;
			case 2:
				printf("\n Enter the element to be deleted : ");
				scanf(" %d",&item_no);
				root=delet(root,item_no);
				inorder(root);
				getch();
				break;
			case 3:
				printf("\n Inorder traversal of binary tree is : ");
				inorder(root);
				printf("\n Preorder traversal of binary tree is : ");
				preorder(root);
				printf("\n Postorder traversal of binary tree is : ");
				postorder(root);
				getch();
				break;
			case 4:
				printf("\n Search and replace operation in binary tree ");
				root=search(root);
				inorder(root);
				getch();
				break;
			case 5:exit(0);
		}
	}
}

struct tree *insert(struct tree *root, int x)
{
	if(!root)
	{
		root=(struct tree*)malloc(sizeof(struct tree));
		root->info = x;
		root->left = NULL;
		root->right = NULL;
		return(root);
	}
	if(root->info > x)
		root->left = insert(root->left,x);
	else
	root->right = insert(root->right,x);
	
	return(root);
}

void inorder(struct tree *root)
{
	if(root != NULL)
	{
		inorder(root->left);
		printf(" %d",root->info);
		inorder(root->right);
	}
	return;
}

void postorder(struct tree *root)
{
	if(root != NULL)
	{
		postorder(root->left);
		postorder(root->right);
		printf(" %d",root->info);
	}
	return;
}

void preorder(struct tree *root)
{
	if(root != NULL)
	{
		printf(" %d",root->info);
		preorder(root->left);
		preorder(root->right);
	}
	return;
}
struct tree *delet(struct tree *ptr,int x)
{
	struct tree *p1,*p2;
	if(!ptr)
	{
		printf("\n Node not found ");
		return(ptr);
	}
	else
	{
		if(ptr->info < x)
		{
			ptr->right = delet(ptr->right,x);
		}
		else if (ptr->info >x)
		{
			ptr->left=delet(ptr->left,x);
			return ptr;
		}
		else
		{
			if(ptr->info == x)
			{
				if(ptr->left == ptr->right)
				{
					free(ptr);
					return(NULL);
				}
				else if(ptr->left==NULL)
				{
					p1=ptr->right;
					free(ptr);
					return p1;
				}
				else if(ptr->right==NULL)
				{
					p1=ptr->left;
					free(ptr);
					return p1;
				}
				else
				{
					p1=ptr->right;
					p2=ptr->right;
					while(p1->left != NULL)
						p1=p1->left;
					p1->left=ptr->left;
					free(ptr);
					return p2;
				}
			}
		}
	}
	return(ptr);
}
struct tree *search(struct tree *root)
{
	int no,i,ino;
	struct tree *ptr;
	ptr=root;
	printf("\n Enter the element to be searched :");
	scanf(" %d",&no);
	fflush(stdin);
	while(ptr)
	{
		if(no>ptr->info)
			ptr=ptr->right;
		else if(no<ptr->info)
			ptr=ptr->left;
		else
			break;
	}
	if(ptr)
	{
		printf("\n Element %d which was searched is found and is = %d",no,ptr->info);
		printf("\n Do you want replace it, press 1 for yes : ");
		scanf(" %d",&i);
		if(i==1)
		{
			printf("\n Enter new element :");
			scanf(" %d",&ino);
			ptr->info=ino;
		}
		else
			printf("\n\t It's okay");
	}
	else
	printf("\n Element %d does not exist in the binary tree",no);
	return(root);
}
