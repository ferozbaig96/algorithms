#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

struct emp
{
	int eid;
	char *ename;
	struct emp *link;
}*start,*t;

void insertion_front(int id,char *name)
{
	struct emp *newEmp=(struct emp*)malloc(sizeof(struct emp));

	if(newEmp==NULL)
	printf("\nOVERFLOW");

	else
	{
		newEmp->eid=id;
		newEmp->ename=name;
		newEmp->link=start;

		start=newEmp;
	}

}

void deletion_rear()
{
	int id;
	char *name;
	struct emp *temp; 

	if(start==NULL)
	printf("\nUNDERFLOW");

	else
	{
		t=start;
		
		while(t->link!=NULL)
		{
			temp=t;
			t=t->link;
		}
		
		id=t->eid;
		name=t->ename;
		
		temp->link=NULL;
		free(t);

		printf("\nID : %d",id);
		printf("\nName : %s\n",name);
	}

}

void display()
{
	if(start==NULL)
	printf("\nUNDERFLOW");

	else
	{
		t=start;

		while(t)
		{
			printf("%d\t%s \n",t->eid,t->ename);
			t=t->link;
		}
	}
}

int main(void)
{
	int i,choice,id;
	char *name;

	clrscr();
	do
	{
		printf("MENU : \n 1.Insertion \n 2.Deletion \n 3.Display \n Enter your choice : ");
		scanf("%d",&choice);

		switch(choice)
		{
			case 1:printf("\nEnter ID : ");
			scanf("%d",&id);
			printf("Enter name : ");
			name=(char*)malloc(50*sizeof(char));
			scanf("%s",name);
			insertion_front(id,name);
			break;

			case 2:deletion_rear();break;

			case 3:display();break;

			default:printf("\nWrong choice");

		}

	printf("\nEnter 1 to continue : ");
	scanf("%d",&i);

	}while(i==1);

	getch();
	return 0;
}
