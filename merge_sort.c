#include <stdio.h>
#include <stdlib.h>

int *arr,n;

void input()
{
	int i;

	for(i=0;i<n;i++)
		scanf("%d",&arr[i]);
}

void disp()
{
	int i;

	for(i=0;i<n;i++)
		printf("%d ",arr[i]);
}


void merge(int *z, int l, int m, int r)

{
	int *left,*right;
	int i,j,c;
	int left_size = m -l +1;
	int right_size = r-m;

	c = l;
	
	left = (int*)malloc(left_size*sizeof(int));
	right = (int*)malloc(right_size*sizeof(int));

	for (i=0 ; i < left_size; i++)
		left[i]=z[l + i];

	for(i=0; i< right_size; i++)
		right[i]=z[i + m + 1];

	i=0;
	j=0;

	while ( i < left_size && j < right_size)
	{

		if ( left[i] < right[j])
			arr[c++] = left[i++];

		else
			arr[c++] = right[j++];
	}

	while ( i < left_size)
		arr[c++]=left[i++];

	while ( j < right_size)
		arr[c++]=right[j++];
}

void merge_sort(int *z,int l,int r)
{
	int m;

	if (l < r)
	{
		m= (l + r)/2;

		merge_sort(z,l,m);
		merge_sort(z,m+1,r);

		merge(z,l,m,r);
	}
}

int main()
{
	printf("Enter the array size : ");
	scanf("%d",&n);

	arr = (int *)malloc(n * sizeof(int));

	printf("Populate the array : \n");
	input();

	printf("Array : \n");
	disp();

	printf("\nArray after sorting : \n");
	merge_sort(arr,0,n-1);
	disp();

	return 0;
}
