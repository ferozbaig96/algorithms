#include <stdio.h>
#include <string.h>
#include <math.h>

char P[100], T[100];
int m, n;

void rabin_karp_matcher(int d, int q)
{
	int h, p, t, s;
	int i;

	m = strlen(P) - 1;	// handling of \n at the end
	n = strlen(T) - 1;	// handling of \n at the end

	p = 0;
	t = 0;
	h = ((int)(pow(d, m-1))) % q;

	//Preprocessing
	for (i = 0; i < m; ++i)
	{
		p = (d*p + P[i] - '0' ) % q;
		t = (d*t + T[i] - '0' ) % q;
	}

	//Matching
	for (s = 0; s <= n-m ; s++)
	{
		if (p == t)
		{
			for(i = 0; i < m; i++)
			{
				if(P[i] != T[s+i])
					break;
			}

			if (i == m)
				printf("Pattern occurs at shift %d\n", s);
		}

		if (s < n-m)
		{	
			t = (d * (t - ((T[s]-'0') * h)) + (T[s + m]-'0')) % q;

			if (t < 0)
				t += q;
		}
	}

}

int main(int argc, char const *argv[])
{
	printf("Enter the text : ");
	fgets(T, sizeof(T), stdin);
	printf("Enter the pattern : ");
	fgets(P, sizeof(P), stdin);

	rabin_karp_matcher(10, 13);

	return 0;
}