#include <stdio.h>
#include <stdlib.h>

int *arr, n;

void input(){

	int i;

	for (i = 0; i < n; ++i)
	{
		scanf("%d",&arr[i]);
	}
}

void disp(){

	int i;

	for (i = 0; i < n; ++i)
	{
		printf("%d ",arr[i]);
	}

}

void lin_search(int item){

	int i, flag = 0, pos ;

	for (i = 0; i < n; ++i)
	{
		if (item == arr[i])
		{
			flag=1;
			pos = i+1;
			break;
		}
	}

	if (flag == 1)
		printf("Item %d is present in the array at position %d\n", item, pos );
	else
		printf("Item %d is not present in the array\n", item);
}

int main()
{
	int item;

	printf("Enter the size of the array\n");
	scanf("%d",&n);

	arr=(int*)malloc(n * sizeof(int));

	printf("Populate the array\n");
	input();

	printf("Array : \n");
	disp();

	printf("\n\nEnter the item to be searched : ");
	scanf("%d",&item);

	lin_search(item);

	return 0;
}