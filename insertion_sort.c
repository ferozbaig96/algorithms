#include <stdio.h>
#include <stdlib.h>

int *arr, n;

void input(){

	int i;

	for (i = 0; i < n; ++i)
	{
		scanf("%d",&arr[i]);
	}
}

void disp(){

	int i;

	for (i = 0; i < n; ++i)
	{
		printf("%d ",arr[i]);
	}

}

void insertion_sort()
{
	int i,j,B;

	for(i = 1;i < n;i++)
	{
		B = arr[i];
		j = i-1;

		while (arr[j] > B && j>= 0)
		{
			arr[j+1] = arr[j];
			j--;
		}

		arr[j+1]=B;
	}
}

int main()
{
	printf("Enter the size of the array\n");
	scanf("%d",&n);

	arr = (int*)malloc(n * sizeof(int));

	printf("Populate the array\n");
	input();

	printf("Array : \n");
	disp();

	printf("\nArray after Sorting (insertion sort) \n");
	insertion_sort();
	disp();
	printf("\n");
	
	return 0;
}
