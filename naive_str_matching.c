#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	int m,n;
	int s;
	int i;
	char P[100], T[100];

	printf("Enter the text : ");
	fgets(T, sizeof(T), stdin);
	printf("Enter the pattern : ");
	fgets(P, sizeof(P), stdin);

	m = strlen(P) - 1;	// handling of \n at the end
	n = strlen(T) - 1;	// handling of \n at the end

	for (s = 0; s <= n-m ; s++)
	{
		for(i = 0; i < m; i++)
		{
			if(P[i] != T[s+i])
				break;
		}

		if (i == m)
			printf("Pattern occurs at shift %d\n", s);
	}

	return 0;
}