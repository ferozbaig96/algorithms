#include <stdio.h>
#include <string.h>

char x[100], y[100];
int m, n, c[100][100],b[100][100];

void find_lcs()
{
	int i,j;

	m = strlen(x) - 1;		//handling \n newline character
	n = strlen(y) - 1;		//handling \n newline character

	for (i = 1; i <= m; i++)
		c[i][0] = 0;

	for (j = 0; j <= n; j++)	// j = 0
		c[0][j] = 0;

	for(i = 1; i <= m; i++)
	{
		for(j = 1; j <= n; j++)
		{
			if(x[i-1] == y[j-1])	// x[i-1] & y[j-1] because string is read from 0 index
			{
				c[i][j] = c[i-1][j-1] + 1;
				b[i][j] = 0;		//top-left
			}
			else if (c[i-1][j] >= c[i][j-1])
			{
				c[i][j] = c[i-1][j];
				b[i][j] = 1;		//top
			}
			else
			{
				c[i][j] = c[i][j-1];
				b[i][j] = 2;		//left
			}
		}
	}
}

void print_lcs(int i, int j)
{
	if (i == 0 || j == 0)
		return;

	if(b[i][j] == 0)
	{
		print_lcs(i-1, j-1);
		printf("%c", x[i-1]);		// x[i-1] because string is read from 0 index
	}
	else if(b[i][j] == 1)
		print_lcs(i-1, j);
	else
		print_lcs(i, j-1);
}

int main(int argc, char const *argv[])
{
	printf("Enter the first string : ");
	fgets(x, sizeof(x), stdin);
	printf("Enter the second string : ");
	fgets(y, sizeof(y), stdin);

	find_lcs();

	printf("\nLength of lcm : %d\n", c[m][n]);
	print_lcs(m, n);
	printf("\n");

	return 0;
}