#include <stdio.h>
#include <stdlib.h>

int *arr, n;

void input(){

	int i;

	for (i = 0; i < n; ++i)
	{
		scanf("%d",&arr[i]);
	}
}

void disp(){

	int i;

	for (i = 0; i < n; ++i)
	{
		printf("%d ",arr[i]);
	}

}

int bin_search(int item, int left, int right)
{
	int mid = (left + right)/2;

	if ( left <= right){

		if(item == arr[mid])
			return (mid+1);
		else if (item < arr[mid])
			return bin_search(item, left, mid-1);
		else
			return bin_search(item, mid+1, right);
	}

	else
		return -1;
}

int main()
{
	int item, pos;

	printf("Enter the size of the array\n");
	scanf("%d",&n);

	arr = (int*)malloc(n * sizeof(int));

	printf("Populate the array\n");
	input();

	printf("Array : \n");
	disp();

	printf("\nEnter the item to be searched\n");
	scanf("%d",&item);
	
	pos = bin_search(item, 0, n-1);
	
	if (pos == -1)
		printf("Item %d not present in the array \n", item);
	else
		printf("Item %d present in the array at position %d\n", item, pos);
	return 0;
}
