#include <stdio.h>
#include <string.h>

char P[100], T[100];
int m, n;
int pi[100];

void compute_prefix()
{	
	int k,q;

	pi[1] = 0;
	k = 0;

	for(q = 2; q <= m; q++)
	{
		while (k > 0 && P[k+1] != P[q])
			k = pi[k];

		if (P[k+1] == P[q])
			k++;

		pi[q] = k;
	}
}

void kmp_matcher()
{
	int q, i;

	m = strlen(P) - 1;	// handling of \n at the end
	n = strlen(T) - 1;	// handling of \n at the end

	//Preprocessing
	compute_prefix();

	q = 0;

	for (i = 0; i < n; ++i)
	{
		while (q > 0 && P[q] != T[i])		// P[q]
			q = pi[q];

		if (P[q] == T[i])					// P[q]
			q++;

		if(q==m)
		{
			printf("Pattern found at shift %d\n", (i-m+1) );		// i-m+1
			q = pi[q];
		}
	}


}

int main(int argc, char const *argv[])
{
	printf("Enter the text : ");
	fgets(T, sizeof(T), stdin);
	printf("Enter the pattern : ");
	fgets(P, sizeof(P), stdin);

	//Matching
	kmp_matcher();

	return 0;
}