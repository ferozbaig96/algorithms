#include <stdio.h>
#include <stdlib.h>

int *arr, n;

void input()
{
	int i;

	for (i = 0; i < n; i++)
		scanf("%d",&arr[i]);
}

void disp()
{
	int i;

	for(i=0;i<n;i++)
		printf("%d ",arr[i]);
}

void swap(int *a, int *b)
{
	int temp;

	temp = *a;
	*a = *b;
	*b = temp;
}

int partition(int *a, int p, int r)
{
	int x,i,j;

	x = a[r];
	i = p-1;

	for(j = p; j < r; j++)
	{
		if (a[j] <= x)
		{
			i++;
			swap(&a[i] , &a[j]);
		}
	}

	swap(&a[i+1] , &a[r]);

	return i+1;
}

void quicksort(int *a, int p, int r)
{
	int q;

	if(p < r)
	{
		q = partition(a,p,r);
		quicksort(a,p,q-1);
		quicksort(a,q+1,r);
	}
}

int main(int argc, char const *argv[])
{
	printf("Enter the size of the array\n");
	scanf("%d",&n);

	arr=(int*)malloc(n * sizeof(int));

	printf("Populate the array\n");
	input();

	printf("\nArray : \n");
	disp();

	quicksort(arr,0,n-1);

	printf("\n\nSorted Array : \n");
	disp();
	printf("\n");

	return 0;
}