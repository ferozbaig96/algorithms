#include <stdio.h>
#include <stdlib.h>

int **a,**b,**c;
int r1,r2,c1,c2;

void input(int r,int c,int **x)
{
    int i,j;

    for(i=0; i<r; i++)
    {
        for(j=0; j<c; j++)
        {
            scanf("%d",&x[i][j]);
        }
    }
}

void disp(int r,int c,int **x)
{
    int i,j;

    for(i=0; i<r; i++)
    {
        for(j=0; j<c; j++)
        {
            printf("%d ",x[i][j]);
        }

        printf("\n");
    }
}

void mul()
{
    int i,j,k;

    for(i=0; i<r1; i++)
    {
        for(j=0; j<c2; j++)
        {
            for(k=0; k<c1; k++)
                c[i][j] += a[i][k] * b[k][j];
        }
    }
}

void init_matrix(int r,int c,int ***x){
    int i;

    *x = (int**)malloc(r * sizeof (int*));

    for (i = 0; i < r; ++i){
        (*x)[i] = (int*)malloc(c * sizeof(int));
    }
}

int main()
{
    printf("Enter the order of matrix 1\n");
    scanf("%d%d",&r1,&c1);

    init_matrix(r1,c1,&a);

    printf("Populate matrix 1\n");
    input(r1,c1,a);

    printf("Enter the order of matrix 2\n");
    scanf("%d%d",&r2,&c2);

    init_matrix(r2,c2,&b);

    printf("Populate matrix 2\n");
    input(r2,c2,b);

    printf("\nMatrix 1\n");
    disp(r1,c1,a);

    printf("\nMatrix 2\n");
    disp(r2,c2,b);

    if(c1 != r2)
        printf("\nMultiplication not possible\n");
    else{

        init_matrix(r1,c2,&c);

        mul();

        printf("\nMatrix 3\n");
        disp(r1,c2,c);
    }

    return 0;
}
