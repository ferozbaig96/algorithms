#include <stdio.h>
 
int s[5],f[5];
char c[5];
 
void sort()
{
	int i,j;
	int t;
	char x;
 
	for(i=0;i<4;i++)
	{
		for(j=0;j<4-i;j++)
		{
			if(f[j] > f[j+1])
			{
				t=f[j];
				f[j]=f[j+1];
				f[j+1]=t;
 
				t=s[j];
				s[j]=s[j+1];
				s[j+1]=t;
 
				x=c[j];
				c[j]=c[j+1];
				c[j+1]=x;
			}
		}
	}
}
 
void printSelectedActivities()
{
	int i, temp=f[0];
 
	printf("%c ",c[0]);
 
	for(i=1;i<5;i++)
	{
 
		if (s[i] >= temp)
		{
			temp=f[i];
			printf(", %c ",c[i]);
		}
	}
}
 
int main(void) {
	int i;
 
 	printf("Enter activity name, starting time, finishing time : \n");

	for(i=0;i<5;i++)
	{	
		do
	    	c[i] = getchar();
		while (isspace(c[i]));

		scanf("%d%d",&s[i],&f[i]);
	}
 
	sort();
 
 	printf("\nSelected activities : ");
	printSelectedActivities();
 	printf("\n");

	return 0;
}